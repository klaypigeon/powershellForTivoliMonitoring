﻿#read-me:Build the README.txt file. Describes files in a directory based on #read-me line

$myPath = "X:\gitlab.com\powershellForTivoliMonitoring\ready\*.ps1"
$outFile = "X:\gitlab.com\powershellForTivoliMonitoring\ready\README.txt"

If (Test-Path $outFile){Remove-Item $outFile}

add-content -Path $outFile -Value "Powershell for Tivoli Monitoring"
add-content -Path $outFile -Value "A series of scripts for managing and automating Tivoli Monitoring via powershell.`r`n"
add-content -Path $outFile -Value "Filename : buildReadMe.ps1`r`nDescription : Build the README.txt file. Describes files in a directory based on #read-me line"
(select-string -path $myPath -pattern "read-me"|Sort-Object -Property Filename)|%{
    If ($_.Filename -ne "buildReadMe.ps1"){
        $line = ("`r`n`r`nFilename : " + $_.Filename + "`r`nDescription : " + ($_.Line -replace "#read-me:",""))
        add-content -Path $outFile -Value $line
    }
}