Powershell for Tivoli Monitoring
A series of scripts for managing and automating Tivoli Monitoring via powershell.

Filename : buildReadMe.ps1
Description : Build the README.txt file. Describes files in a directory based on #read-me line


Filename : restartTivNT.ps1
Description : Starts only the remote NT agents which cannot be done with tacmd.
