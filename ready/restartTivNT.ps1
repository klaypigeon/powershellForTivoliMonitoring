﻿#read-me:Starts only the remote NT agents which cannot be done with tacmd.
# Must be run as AD account that has permission to restart server services. 
# Author: theo.macris@gmail.com
# Feel free to tweak!

# EDIT VALUES ########
$tems = "temsserver"
$temsuser = "temsUser"
$temspw = "temsPW"
######################

$list = @()
$bad = @()
$good = @()


$command = "tacmd login -s $tems -u $temsuser -p $temspw -t 100"
Invoke-Expression -command $command


$command = "tacmd listsystems -t NT|grep ' N '"
$status = (Invoke-Expression $command)
$hosts = $status|%{($_.split(":"))[1]}
#write-host ("Attempting to Restart Tivoli Agent on:`r`n" + $hosts)

foreach ($pc in $hosts){
    #write-host $pc
    if (Test-Connection -Count 1 -TimeToLive 30 -Quiet -ComputerName $pc){
        $good += $pc
        $command = ("sc \\" + $pc + " stop KNTCMA_Primary")
        cmd /c $command

        Start-Sleep -Seconds 5
        $command = ("sc \\" + $pc + " start KNTCMA_Primary")
        cmd /c $command
        }Else{$bad += ("connection not valid to:   " + $pc)}
    }

    Start-Sleep -Seconds 5
    #clear-host

    foreach ($pc in $good){
    $list += (Get-Service -Name KNTCMA_Primary -ComputerName $pc|Where-Object {$_.status -ne "running"})
        }
    $list | fl MachineName,Status,Name -ErrorAction Ignore
    $bad

    $command = "tacmd listsystems -t NT|grep ' N '"
    Invoke-Expression $command
